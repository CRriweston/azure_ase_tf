resource "null_resource" "validate_ilbMode" {
    count = "${
        lower(local.ilbMode) == "public" || "private" ? 0 : 1
    }"

    "${local.error_ilbMode}" = "true"
}

resource "null_resource" "validate_workers" {
    count = "${
        var.workers > 0 && var.workers <= 100 ? 0 : 1
    }"

    "${local.error_workers}" = "true"
}

locals {
    error_ilbMode = <<EOF
The value ${var.ilbMode} for input 'var.ilbMode' is invalid

Valid inputs:
- Public
- Private
EOF

    error_workers = <<EOF
The value ${var.workers} for input 'var.workers' is invalid

Valid inputs:
- Must be a value from 1 to 100
EOF

}