variable "location" {
  default     = "UK West"
  description = "Azure Region"
  type        = "string"
}
variable "subnet_name" {
  description = ""
}

variable "aseName" {
  default     = "appservice-tf"
  description = "Name of the Azure ASE instance"
  type        = "string"
}

variable "appName" {
  description = "Name of the ASE hosted application"
  type        = "string"
}

variable "ilbMode" {
  default     = 0
  description = "Public (0) or Private (1) ASE instance"
  type        = "string"
}

variable "workers" {
  default     = 0
  description = "Number of workers the service plan will provide"
  type        = "string"
}
