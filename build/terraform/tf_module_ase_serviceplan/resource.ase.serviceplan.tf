resource "azurerm_app_service_plan" "main" {
  name                = "${var.aseName}-${var.appName}-service-plan"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  app_service_environment_id = "${azurerm_template_deployment.main.outputs["app_service_evironment_id"]}"

  sku {
    tier     = "Isolated"
    size     = "${
      lower(var.ilbMode) == "small" ?
      "I1" : false ||
      lower(var.ilbMode) == "medium" ?
      "Ï2" : false ||
      lower(var.ilbMode) == "large" ?
      "Ï3" : false
    }"
    capacity = "${var.workers}"
  }
}
