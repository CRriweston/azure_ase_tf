data "azurerm_subnet" "main" {
  name                 = "SNET-ase-lab-tf"
  virtual_network_name = "${data.azurerm_subnet.main.name}"
  resource_group_name  = "${azurerm_resource_group.main.name}"
}
